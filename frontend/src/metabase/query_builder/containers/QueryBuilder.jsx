import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import cx from "classnames";
import _ from "underscore";

import { AngularResourceProxy } from "metabase/lib/redux";
import { loadTableAndForeignKeys } from "metabase/lib/table";
import { isPK, isFK } from "metabase/lib/types";

import NotFound from "metabase/components/NotFound.jsx";
import QueryHeader from "../QueryHeader.jsx";
import GuiQueryEditor from "../GuiQueryEditor.jsx";
import NativeQueryEditor from "../NativeQueryEditor.jsx";
import QueryVisualization from "../QueryVisualization.jsx";
import DataReference from "../dataref/DataReference.jsx";
import TagEditorSidebar from "../template_tags/TagEditorSidebar.jsx";
import QueryBuilderTutorial from "../../tutorial/QueryBuilderTutorial.jsx";
import SavedQuestionIntroModal from "../SavedQuestionIntroModal.jsx";
import Query from "metabase/lib/query";
import Recorder from "../libs/recorder";

import {
    card,
    originalCard,
    databases,
    queryResult,
    parameterValues,
    isDirty,
    isNew,
    isObjectDetail,
    tables,
    tableMetadata,
    tableForeignKeys,
    tableForeignKeyReferences,
    uiControls,
    getParameters,
    getDatabaseFields,
    getSampleDatasetId,
    getFullDatasetQuery,
} from "../selectors";

import * as actions from "../actions";
import { push } from "react-router-redux";

const cardApi = new AngularResourceProxy("Card", ["create", "update", "delete"]);
const dashboardApi = new AngularResourceProxy("Dashboard", ["list", "create"]);
const revisionApi = new AngularResourceProxy("Revision", ["list", "revert"]);
const Metabase = new AngularResourceProxy("Metabase", ["db_autocomplete_suggestions"]);

function cellIsClickable(queryResult, rowIndex, columnIndex) {
    if (!queryResult) return false;

    // lookup the coldef and cell value of the cell we are curious about
    var coldef = queryResult.data.cols[columnIndex];

    if (!coldef || !coldef.special_type) return false;

    return (coldef.table_id != null && (isPK(coldef.special_type) || (isFK(coldef.special_type) && coldef.target)));
}

function autocompleteResults(card, prefix) {
    let databaseId = card && card.dataset_query && card.dataset_query.database;
    let apiCall = Metabase.db_autocomplete_suggestions({
       dbId: databaseId,
       prefix: prefix
    });
    return apiCall;
}

const mapStateToProps = (state, props) => {
    return {
        user:                      state.currentUser,
        fromUrl:                   props.location.query.from,
        location:                  props.location,

        card:                      card(state),
        originalCard:              originalCard(state),
        query:                     state.qb.card && state.qb.card.dataset_query,  // TODO: EOL, redundant
        parameterValues:           parameterValues(state),
        databases:                 databases(state),
        tables:                    tables(state),
        tableMetadata:             tableMetadata(state),
        tableForeignKeys:          tableForeignKeys(state),
        tableForeignKeyReferences: tableForeignKeyReferences(state),
        result:                    queryResult(state),
        isDirty:                   isDirty(state),
        isNew:                     isNew(state),
        isObjectDetail:            isObjectDetail(state),
        uiControls:                uiControls(state),
        parameters:                getParameters(state),
        databaseFields:            getDatabaseFields(state),
        sampleDatasetId:           getSampleDatasetId(state),
        fullDatasetQuery:          getFullDatasetQuery(state),

        isShowingDataReference:    state.qb.uiControls.isShowingDataReference,
        isShowingTutorial:         state.qb.uiControls.isShowingTutorial,
        isEditing:                 state.qb.uiControls.isEditing,
        isRunning:                 state.qb.uiControls.isRunning,
        cardApi:                   cardApi,
        dashboardApi:              dashboardApi,
        revisionApi:               revisionApi,
        loadTableAndForeignKeysFn: loadTableAndForeignKeys,
        autocompleteResultsFn:     (prefix) => autocompleteResults(state.qb.card, prefix),
        cellIsClickableFn:         (rowIndex, columnIndex) => cellIsClickable(state.qb.queryResult, rowIndex, columnIndex)
    }
}

const getURL = (location) =>
    location.pathname + location.search + location.hash;


const mapDispatchToProps = {
    ...actions,
    onChangeLocation: push
};

@connect(mapStateToProps, mapDispatchToProps)
export default class QueryBuilder extends Component {

    constructor(props, context) {
        super(props, context);

        _.bindAll(
            this,
            'handleResize',
            'handleRecord',
            'initRecorder',
            'execRecordQuery',
            'decodeOgg',
            'convertToBase64',
            'sendRequest'
        );

        // TODO: React tells us that forceUpdate() is not the best thing to use, so ideally we can find a different way to trigger this
        this.forceUpdateDebounced = _.debounce(this.forceUpdate.bind(this), 400);

        this.state = {
            recordStatus: false,
            recordImage: '/app/components/icons/assets/before_record'
        };

        this.recorder = new Recorder({
            numberOfChannels: 1,
            encoderSampleRate: 16000,
            encoderPath: "/app/assets/js/encoderWorker.min.js"
        });
    }

    componentWillMount() {
        this.props.initializeQB(this.props.location, this.props.params);
        this.initRecorder();
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.uiControls.isShowingDataReference !== this.props.uiControls.isShowingDataReference ||
            nextProps.uiControls.isShowingTemplateTagsEditor !== this.props.uiControls.isShowingTemplateTagsEditor) {
            // when the data reference is toggled we need to trigger a rerender after a short delay in order to
            // ensure that some components are updated after the animation completes (e.g. card visualization)
            window.setTimeout(this.forceUpdateDebounced, 300);
        }

        if (nextProps.location.action === "POP" && getURL(nextProps.location) !== getURL(this.props.location)) {
            this.props.popState(nextProps.location);
        } else if (this.props.location.query.tutorial === undefined && nextProps.location.query.tutorial !== undefined) {
            this.props.initializeQB(nextProps.location, nextProps.params);
        } else if (getURL(nextProps.location) === "/q" && getURL(this.props.location) !== "/q") {
            this.props.initializeQB(nextProps.location, nextProps.params);
        }
    }

    componentDidUpdate() {
        let viz = ReactDOM.findDOMNode(this.refs.viz);
        if (viz) {
            viz.style.opacity = 1.0;
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    // When the window is resized we need to re-render, mainly so that our visualization pane updates
    // Debounce the function to improve resizing performance.
    handleResize(e) {
        this.forceUpdateDebounced();
        let viz = ReactDOM.findDOMNode(this.refs.viz);
        if (viz) {
            viz.style.opacity = 0.2;
        }
    }

    execRecordQuery(queryString) {
        queryString = queryString.toLowerCase();

        if (!queryString.includes('orders') || !queryString.includes('last week')) {
            console.log('keywords not found');
        }

        else {
            // Update source table so that filters and aggregator can be used
            this.props.setSourceTableFn(1);

            // Update aggregation manually
            let query = this.props.query.query;

            if (queryString.includes('count')) {
                console.log('contains count');

                Query.updateAggregation(query, ["count"]);
            }

            Query.addFilter(query);
            Query.updateFilter(query, Query.getFilters(query).length - 1, ["TIME_INTERVAL", 1, "last", "week"]);

            this.props.query.database = 1;
            this.props.query.query.source_table = 1;

            // Set the query
            this.props.setQueryFn(this.props.query);

            // Runs the query
            this.props.runQueryFn();
        }
    }

    sendRequest(base64String) {
        console.log('Sending request...');

        let execRecordQuery = this.execRecordQuery;

        // For sending post request
        var http = new XMLHttpRequest();
        var url = "https://speech.googleapis.com/v1beta1/speech:syncrecognize?key=AIzaSyDE0VKAow7tF_vKMfQGIesaWoKDEpYTD9k"

        // Opening the request
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/json");

        // On receiving response update display
        http.onreadystatechange = function() {
            console.log('State changed');
            if(http.readyState == 4 && http.status == 200) {
                var res = JSON.parse(http.responseText);

                // If result is obtained as expected
                if (
                    res.results !== undefined &&
                    res.results !== null &&
                    res.results.length > 0 &&
                    res.results[0].alternatives.length > 0
                ) {
                    let firstAlt = res.results[0].alternatives[0];
                    let resultString = firstAlt.transcript;

                    console.log(resultString + ' with ' + (firstAlt.confidence * 100) + '% confidece');

                    execRecordQuery(resultString);
                }

                else {
                    console.log('Result not found! :( Try again :)');
                }
            }

            else {
                console.log('Seems like there was an error');
            }
        };

        var requestBody = JSON.stringify({
            config: {
                encoding: "LINEAR16",
                sampleRate: "16000",
                languageCode:"en-US"
            },
            audio: {
                content: base64String
            }
        });

        http.send(requestBody);
    }

    convertToBase64(blob) {
        console.log('Converting to base 64...');

        let sendRequest = this.sendRequest;

        var reader = new window.FileReader();

        reader.readAsDataURL(blob);

        reader.onloadend = function() {
            sendRequest(reader.result.substr(reader.result.indexOf(',') + 1));
        }
    }

    decodeOgg(arrayBuffer) {
        console.log('Decoding...');

        let convertToBase64 = this.convertToBase64;

        var typedArray = new Uint8Array(arrayBuffer);
        var decoderWorker = new Worker('/app/assets/js/decoderWorker.min.js');
        var wavWorker = new Worker('/app/assets/js/waveWorker.min.js');

        decoderWorker.postMessage({
            command:'init',
            decoderSampleRate: 16000,
            outputBufferSampleRate: 16000
        });

        wavWorker.postMessage({
            command:'init',
            bitDepth: 16,
            sampleRate: 16000
        });

        decoderWorker.onmessage = function(e){
            // null means decoder is finished
            if (e.data === null) {
                wavWorker.postMessage({ command: 'done' });
            }

            // e.data contains decoded buffers as float32 values
            else {
                wavWorker.postMessage({
                    command: 'record',
                    buffers: e.data
                }, e.data.map(function(typedArray){
                    return typedArray.buffer;
                }));
            }
        };

        wavWorker.onmessage = function(e){
            var dataBlob = new Blob( [ e.data ], { type: "audio/wav" } );

            convertToBase64(dataBlob);
        };

        decoderWorker.postMessage({
            command: 'decode',
            pages: typedArray
        }, [typedArray.buffer] );

        decoderWorker.postMessage({
            command: 'done'
        });
    }

    initRecorder() {
        console.log('Initialising recorder...');

        let decodeOgg = this.decodeOgg;

        if (!Recorder.isRecordingSupported()) {
            console.log('Some features are not supported in your browser');
        }

        this.recorder.addEventListener( "streamError", function(e){
            console.log('Error encountered: ' + e.error.name);
        });

        this.recorder.addEventListener( "streamReady", function(e){
            console.log('Audio stream is ready.');
        });

        this.recorder.addEventListener( "dataAvailable", function(e){
            console.log('Data available...');
            decodeOgg(e.detail);
        });

        this.recorder.initStream();
    }

    handleRecord() {
        let status = !this.state.recordStatus;
        this.setState({ recordStatus: !this.state.recordStatus });

        if (status) {
            console.log('Start recording');

            this.recorder.start();

            this.setState({recordImage: '/app/components/icons/assets/while_record'});
        } else {
            console.log('Stop recording');

            this.recorder.stop();

            this.setState({recordImage: '/app/components/icons/assets/before_record'});
        }
    }

    render() {
        const { card, isDirty, databases, uiControls } = this.props;

        // if we can't load the card that was intended then we end up with a 404
        // TODO: we should do something more unique for is500
        if (uiControls.is404 || uiControls.is500) {
            return (
                <div className="flex flex-column flex-full layout-centered">
                    <NotFound />
                </div>
            );
        }

        // if we don't have a card at all or no databases then we are initializing, so keep it simple
        if (!card || !databases) {
            return (
                <div></div>
            );
        }

        const showDrawer = uiControls.isShowingDataReference || uiControls.isShowingTemplateTagsEditor;
        return (
            <div className="flex-full relative">
                <div className={cx("QueryBuilder flex flex-column bg-white spread", {"QueryBuilder--showSideDrawer": showDrawer})}>
                    <div id="react_qb_header">
                        <QueryHeader {...this.props}/>
                    </div>

                    <div id="react_qb_editor" className="z2">
                        { card && card.dataset_query && card.dataset_query.type === "native" ?
                            <NativeQueryEditor {...this.props} isOpen={!card.dataset_query.native.query || isDirty} />
                        :
                            <div className="wrapper"><GuiQueryEditor {...this.props}/></div>
                        }
                    </div>

                    <div ref="viz" id="react_qb_viz" className="flex z1" style={{ "transition": "opacity 0.25s ease-in-out" }}>
                        <QueryVisualization {...this.props}/>
                    </div>
                </div>

                <div className={cx("SideDrawer", { "SideDrawer--show": showDrawer })}>
                    { uiControls.isShowingDataReference &&
                        <DataReference {...this.props} closeFn={() => this.props.toggleDataReference()} />
                    }

                    { uiControls.isShowingTemplateTagsEditor &&
                        <TagEditorSidebar {...this.props} onClose={() => this.props.toggleTemplateTagsEditor()} />
                    }
                </div>

                <img src={`${this.state.recordImage}.png`} height="250px" className="record-button" onClick={this.handleRecord}/>

                { uiControls.isShowingTutorial &&
                    <QueryBuilderTutorial onClose={() => this.props.closeQbTutorial()} />
                }

                { uiControls.isShowingNewbModal &&
                    <SavedQuestionIntroModal onClose={() => this.props.closeQbNewbModal()} />
                }
            </div>
        );
    }
}
